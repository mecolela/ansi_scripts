# Ansible scripts

A project listing out useful playbooks to automate server admin stuff

```bash
ansible-playbook the-playbook.yml --key-file [path to your ssh key file]
```

## Nginx + Certbot playbook

The [Web server setup](web-server-setup.yml) playbook runs an update and
upgrade for your server, then installs nginx and certbot accordinly.

It'll then fire up certbot to prepare an ssl certificate and edits nginx
accordingly.
